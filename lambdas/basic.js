let add = function(a, b) {
    return a + b;
}

let addLambda1 = (a, b) => {
    return a + b;
}

let addLambda2 = (a, b) =>

a + b;

let echo = function(value) {
    return value;
}

let echoLambda = v => v * 2;

console.log(addLambda2(1, 4));
console.log(echoLambda(18));