class Animal {
    constructor(name) {
        this.name = name;
    }

    displayNameDelayed() {
        this.name = "Raichu";
        setTimeout(()  => {
            
            console.log(this.name);
        }, 3000);
    }
}

let animal = new Animal("Pikachu");
animal.displayNameDelayed();