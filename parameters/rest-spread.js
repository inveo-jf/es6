// Rest operator
function greetMany(greeting, ...people) {
    
    for (let person of people) {
        console.log(greeting, person);
    }
}

let people = ["Oliver", "Slade", "Barry"];
// Spread operator
// Co se stane?
greetMany("Good morning", ...people);