// Rodicovska trida
function Animal(name) {
    this.name = name;
    // Return? return { name: "foo"}
}
Animal.prototype.getName = function() {
    return this.name;
};
var animal = new Animal("Pikachu");
// Jak se instance zvirete dozvi o prototypu?
// animal.__proto__ == Animal.prototype;

// Podedime zvire
function Dog(name) {
    // Jako :base(name) v C#
    Animal.call(this, name);
}

Dog.prototype = Object.create(Animal.prototype);
Dog.prototype.getName = function() {
    return "Dog " + Animal.prototype.getName.call(this);
};

let dog = new Dog("Rex");
console.log(dog instanceof Animal);
console.log(dog.getName());

// Zkusime prepsat getName
Animal.prototype.getName = function() {
    return "Nova hodnota";
};