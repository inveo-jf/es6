function Animal(name) {
    this.name = name;
}

Animal.prototype.getName = function() {
    return this.name;
}

class Dog extends Animal {
    constructor(name) {
        super(name);
    }

    getName() {
        return "Dog " + super.getName();
    }
}

let animal = new Animal("Pikachu");
let dog = new Dog("Rex");
console.log(animal.getName());
console.log(dog.getName());