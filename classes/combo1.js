class Animal {
    constructor(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }
}

Animal.prototype.getNames = function() {
    return "Radsi to nekombinuj, nebo se v tom nevyznas!";
}

let animal = new Animal("Rex");
console.log(animal.name);
console.log(animal.getNames());