class Animal {
    constructor(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }
}

function Dog(name) {
    Animal.call(this, name);
}

Dog.prototype = Object.create(Animal.prototype);

let dog = new Dog("Rex");
console.log(dog.getName());