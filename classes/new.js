class Animal {
    constructor(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }
}

class Dog extends Animal {
    constructor(name) {
        super(name); // jako :base(name) v C#
    }
    getName() {
        return "Dog " + super.getName();
    }
}

let dog = new Dog("Rex");
console.log(dog instanceof Animal);
console.log(dog.getName());
