var animals = ["Pikachu, Charmander, Mewtwo"];
var obj={first: "john", last:"doe"};

function oldFor() {
    for (let i = 0; i < animals.length; i++) {
        console.log(animals[i]);
    }
}

function forIn() {
    for (let prop in obj) {
        console.log(obj[prop]);
    }
}

// ES6
function forOf() {
    for (let animal of animals) {
        console.log(animal);
    }
}

// oldFor();
animals.forEach(function(value){
console.log()

})

forIn();
forOf();